//
//  AGTNotesViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 23/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTNotesViewController.h"
#import "AGTNote.h"
#import "AGTPhoto.h"
#import "AGTNoteViewController.h"

@implementation AGTNotesViewController


-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Averiguamos la nota
    AGTNote * note = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Creamos la celda
    static NSString *cellId = @"noteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        // la creo de cero
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    }
    
    // la configuramos
    cell.textLabel.text = note.name;
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateStyle = NSDateIntervalFormatterLongStyle;
    cell.detailTextLabel.text = [fmt stringFromDate:note.modificationDate];
    cell.imageView.image = note.photo.image;
    
    // la devolvemos
    return cell;
    
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Averiguamos la nota
    AGTNote *n = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Creamos una instancia del contorlador de notas
    AGTNoteViewController *nVC = [[AGTNoteViewController alloc] initWithModel:n];
    
    // Hacemos el push
    [self.navigationController pushViewController:nVC
                                         animated:YES];
    
    
}












@end








