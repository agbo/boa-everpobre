//
//  UIViewController+Navigation.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 23/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Navigation)

-(UINavigationController*)wrappedInNavigation;


@end
