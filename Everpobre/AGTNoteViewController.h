//
//  AGTNoteViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 23/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

@class AGTNote;


@interface AGTNoteViewController : UITableViewController

@property (nonatomic, strong) AGTNote *model;

-(id) initWithModel:(AGTNote*) model;

@end
