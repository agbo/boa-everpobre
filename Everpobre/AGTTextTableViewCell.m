//
//  AGTTextTableViewCell.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 23/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTTextTableViewCell.h"
#import "AGTNote.h"

@implementation AGTTextTableViewCell

+(CGFloat) height{
    return 220.0;
}

-(void) setNote:(AGTNote *)note{
    
    // asigno nota
    _note = note;
    
    // me hago delegado y copio el texto a
    // la textview
    if (_note != nil) {
        self.text.text = _note.text;
        self.text.delegate = self;
    }
}

-(void)prepareForReuse{
    self.note = nil;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UITextViewDelegate
-(void) textViewDidChange:(UITextView *)textView{
    
    // El texto ha cambiado
    self.note.text = textView.text;
    
}













@end
