//
//  AGTTextTableViewCell.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 23/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

@class AGTNote;

@interface AGTTextTableViewCell : UITableViewCell <UITextViewDelegate>

+(CGFloat) height;
@property (weak, nonatomic) IBOutlet UITextView *text;

@property (nonatomic, strong) AGTNote *note;




@end
