#import "AGTPhoto.h"

@interface AGTPhoto ()

// Private interface goes here.

@end

@implementation AGTPhoto

-(UIImage *) image{
    // Convierte photoData (un NSData) en un UIImage
    UIImage *img = [UIImage imageWithData:self.photoData];
    return img;
}

-(void) setImage:(UIImage *)image{
    
    // Convertir un UIImage en un NSData
    self.photoData = UIImageJPEGRepresentation(image, 0.9);
    
}
@end












