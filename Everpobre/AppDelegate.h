//
//  AppDelegate.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 22/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGTCoreDataStack.h"
#import "Settings.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AGTCoreDataStack *stack;


@end

