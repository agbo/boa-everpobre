//
//  AGTDatesTableViewCell.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 23/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

@class AGTNote;

@interface AGTDatesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *modificationDate;
@property (weak, nonatomic) IBOutlet UILabel *creationDate;

+(CGFloat) height;

-(void) observeNote:(AGTNote*) note;

@end
