//
//  AGTPhotoViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 23/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AGTNote;

@interface AGTPhotoViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) AGTNote* model;

-(id) initWithModel:(AGTNote*) note;
- (IBAction)takePicture:(id)sender;

- (IBAction)trashPhoto:(id)sender;


@end
