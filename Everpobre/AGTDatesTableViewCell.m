//
//  AGTDatesTableViewCell.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 23/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTDatesTableViewCell.h"

#import "AGTNote.h"

@interface AGTDatesTableViewCell ()
@property (nonatomic, strong) AGTNote *note;
@end


@implementation AGTDatesTableViewCell

+(CGFloat) height{
    return 55.0;
}

-(void) observeNote:(AGTNote*) note{
    
    // Guardamos la nota
    self.note = note;
    
    // Observamos las propiedades de fecha
    [self.note addObserver:self
                forKeyPath:AGTNoteAttributes.creationDate
                   options:0
                   context:NULL];
    
    [self.note addObserver:self
                forKeyPath:AGTNoteAttributes.modificationDate
                   options:0
                   context:NULL];
    
    // Sincronizamos
    [self syncWithNote];
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    [self syncWithNote];
}

-(void) dealloc{
    [self stopObserving];
}

-(void) prepareForReuse{
    
    [self stopObserving];
    self.note = nil;
}

-(void) stopObserving{
    
    [self.note removeObserver:self
                   forKeyPath:AGTNoteAttributes.creationDate];
    
    [self.note removeObserver:self
                   forKeyPath:AGTNoteAttributes.modificationDate];
}

-(void) syncWithNote{
    
    
    NSDateFormatter *fmt = [NSDateFormatter new];
    fmt.dateStyle = NSDateFormatterShortStyle;
    
    self.modificationDate.text = [fmt stringFromDate:self.note.modificationDate];
    
    self.creationDate.text = [fmt stringFromDate:self.note.creationDate];
    
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}







@end
