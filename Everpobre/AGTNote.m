#import "AGTNote.h"
#import "AGTPhoto.h"
@interface AGTNote ()

+(NSArray*) observableKeys;

@end

@implementation AGTNote

#pragma mark - Class methods
+(instancetype) noteWithName:(NSString *) name
                     context:(NSManagedObjectContext *) context{
    
    AGTNote *n = [NSEntityDescription insertNewObjectForEntityForName:[self entityName] inManagedObjectContext:context];
    
    n.creationDate = [NSDate date];
    n.name = name;
    n.photo = [AGTPhoto insertInManagedObjectContext:context];
    n.modificationDate = [NSDate date];
    
    return n;
}

+(NSArray*) observableKeys{
    return @[@"name", @"text", @"photo", @"photo.photoData"];
}

#pragma mark - KVO
-(void) setupKVO{
    
    // Alta en notificaciones
    for (NSString *key in [AGTNote observableKeys]) {
        [self addObserver:self
               forKeyPath:key
                  options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                  context:NULL];
    }
    
}

-(void) tearDownKVO{
    // Baja en todas las notificaciones
    for (NSString *key in [AGTNote observableKeys]) {
        
        [self removeObserver:self
                  forKeyPath:key];
    }
}

// Recepción de la notificación
-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary *)change
                      context:(void *)context{
    // sea quien sea que haya cambiado, actualizo la modificationDate
    self.modificationDate = [NSDate date];
}

#pragma mark - Life cycle
-(void) awakeFromInsert{
    [super awakeFromInsert];
    // Se llama solo una vez, al crear el objeto
    [self setupKVO];
}

-(void) awakeFromFetch{
    [super awakeFromFetch];
    // Se llama cada vez que se recupera el objeto
    [self setupKVO];
}


-(void) willTurnIntoFault{
    [super willTurnIntoFault];
    // Se llama cada vez que un objeto se va a convertir en un fault
    [self tearDownKVO];
}

@end

















