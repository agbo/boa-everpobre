//
//  AGTNotebooksViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 22/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTNotebooksViewController.h"
#import "AGTNotebook.h"
#import "AGTNotesViewController.h"
#import "AGTNote.h"

@interface AGTNotebooksViewController ()

@end

@implementation AGTNotebooksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Ponemos un título
    self.title = @"Everpobre";
    
    // Añadimos botón de añadir libreta
    [self addNewNotebookButton];
    
    // Botón de edición
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    
}

-(void) addNewNotebookButton{
    
    // Creamos un botón de barra
    UIBarButtonItem * btn = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                target:self
                             action:@selector(addNotebook:)];
    
    // lo pongo al lado derecho
    self.navigationItem.rightBarButtonItem = btn;
    
}

-(void) addNotebook:(id) sender{
    
    [AGTNotebook notebookWithName:@"Lugares donde me pasaron cosas raras" context:self.fetchedResultsController.managedObjectContext];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Averiguar de qué libreta me habla
    AGTNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    // Creo una celda
    static NSString *cellId = @"NotebookCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    // La configuro
    cell.textLabel.text = nb.name;
    
    // La devuelvo
    return cell;
}


-(void) tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Borrando una celda
        
        // Averiguo cual es la libreta
        AGTNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        // la borro del modelo
        [self.fetchedResultsController.managedObjectContext deleteObject:nb];
        
    }
    
}

-(NSString*) tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return @"Exterminar";
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // Descubrir cual es la libreta
    AGTNotebook *nb = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Crear el fetch request con su predicado
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[AGTNote entityName]];
    
    req.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:AGTNoteAttributes.name ascending:YES selector:@selector(caseInsensitiveCompare:)]];
    
    req.fetchBatchSize = 20;
    req.predicate = [NSPredicate predicateWithFormat:@"notebook == %@", nb];
    
    
    // Creamos el fetchedResultsController
    NSFetchedResultsController *fc = [[NSFetchedResultsController alloc]initWithFetchRequest:req managedObjectContext:self.fetchedResultsController.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    
    // Creamos el controlador de tabla de notas
    AGTNotesViewController *notesVC = [[AGTNotesViewController alloc] initWithFetchedResultsController:fc style:UITableViewStylePlain];
    
    // Hacemos un push
    [self.navigationController pushViewController:notesVC
                                         animated:YES];
    
}







@end














