#import "_AGTPhoto.h"
@import UIKit;

@interface AGTPhoto : _AGTPhoto {}
// Custom logic goes here.

@property (nonatomic, strong) UIImage *image;

@end
