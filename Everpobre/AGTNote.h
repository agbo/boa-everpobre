#import "_AGTNote.h"

@interface AGTNote : _AGTNote {}
// Custom logic goes here.

+(instancetype) noteWithName:(NSString *) name
                     context:(NSManagedObjectContext *) context;

@end
