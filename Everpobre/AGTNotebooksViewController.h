//
//  AGTNotebooksViewController.h
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 22/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTCoreDataTableViewController.h"

@interface AGTNotebooksViewController : AGTCoreDataTableViewController

@end
