//
//  AGTNoteViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 23/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTNoteViewController.h"
#import "AGTNote.h"
#import "AGTDatesTableViewCell.h"
#import "AGTTextTableViewCell.h"
#import "AGTPhotoViewController.h"

#define DATE_SECTION 0
#define TEXT_SECTION 1

#define DATE_CELL_ID @"DATE CELL ID"
#define TEXT_CELL_ID @"TEXT CELL ID"


@interface AGTNoteViewController ()

@end

@implementation AGTNoteViewController

-(id) initWithModel:(AGTNote*) model{
    
    if (self = [super initWithStyle:UITableViewStyleGrouped]) {
        
        _model = model;
        self.title = model.name;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    // Registramos los nibs de las celdas personalizadas
    UINib *dateCellNib = [UINib nibWithNibName:@"AGTDatesTableViewCell"
                                        bundle:nil];
    
    [self.tableView registerNib:dateCellNib
         forCellReuseIdentifier:DATE_CELL_ID];
    
    
    UINib *textCellNib = [UINib
                          nibWithNibName:@"AGTTextTableViewCell"
                          bundle:nil];
    [self.tableView registerNib:textCellNib
         forCellReuseIdentifier:TEXT_CELL_ID];
    
    // Añadimos botón para ver foto
    UIBarButtonItem *btn = [[UIBarButtonItem alloc]initWithTitle:@"Foto" style:UIBarButtonItemStylePlain target:self action:@selector(displayPhotoViewController:)];
    
    self.navigationItem.rightBarButtonItem = btn;
}

-(void)displayPhotoViewController:(id) sender{
    // continuará, tras el silpancho
    
    AGTPhotoViewController *pVC = [[AGTPhotoViewController alloc] initWithModel:self.model];
    
    [self.navigationController pushViewController:pVC
                                         animated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

-(NSString*) tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section{
    
    
    if (section == DATE_SECTION) {
        return @"Dates:";
    }else {
        return @"Text:";
    }
    
}

-(CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == DATE_SECTION) {
        return [AGTDatesTableViewCell height];
    }else{
        return [AGTTextTableViewCell height];
    }
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.section == DATE_SECTION) {
        
        // Creamos la celda personalizada
        AGTDatesTableViewCell *dCell = [tableView dequeueReusableCellWithIdentifier:DATE_CELL_ID                               forIndexPath:indexPath];
        
        // configurarla
        [dCell observeNote:self.model];
        
        return dCell;
        
    }else{
        // El texto
        AGTTextTableViewCell *tCell = [tableView dequeueReusableCellWithIdentifier:TEXT_CELL_ID forIndexPath:indexPath];
        
        // configurarla
        tCell.note = self.model;
        
        return tCell;
    }
}











@end
