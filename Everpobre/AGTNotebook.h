#import "_AGTNotebook.h"

@interface AGTNotebook : _AGTNotebook {}

+(instancetype)notebookWithName:(NSString*)name
                        context: (NSManagedObjectContext *) context;

@end
