//
//  AppDelegate.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 22/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AppDelegate.h"
#import "AGTNote.h"
#import "AGTPhoto.h"
#import "AGTNotebook.h"
#import "AGTNotebooksViewController.h"
#import "UIViewController+Navigation.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Creamos un Stack de Core Data
    self.stack = [AGTCoreDataStack coreDataStackWithModelName:@"Model"];
    
    //[self makeDummyData];
    
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
   // self.window.backgroundColor = [UIColor greenColor];
    
    // Obtenemos las libretas
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:[AGTNotebook entityName]];
    
    req.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:AGTNotebookAttributes.name ascending:YES]];
    req.fetchBatchSize = 20;
    
    NSFetchedResultsController *res = [[NSFetchedResultsController alloc] initWithFetchRequest:req managedObjectContext:self.stack.context sectionNameKeyPath:nil cacheName:nil];
    
    
    // Creo el controlador
    AGTNotebooksViewController *nbVC = [[AGTNotebooksViewController alloc] initWithFetchedResultsController:res style:UITableViewStylePlain];
    
    
    
    // Lo pongo como root
    self.window.rootViewController = [nbVC wrappedInNavigation];
    
    
    [self.window makeKeyAndVisible];

    // Arranco el autoSave
    [self autoSave];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error al guardar");
    }];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error al guardar");
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) makeDummyData{
    
    // Eliminamos datos existentes
    [self.stack zapAllData];
    
    // Creamos una libreta
    AGTNotebook *exs = [AGTNotebook
                        notebookWithName:@"Ex-Novias para el recuerdo"
                        context:self.stack.context];
    
    // Le añadimos notas
    AGTNote *mariana = [AGTNote noteWithName:@"Mariana Dávalos"
                                     context:self.stack.context];
    AGTNote *camila = [AGTNote noteWithName:@"Camila Dávalos"
                                     context:self.stack.context];
    
    mariana.notebook = exs;
    camila.notebook = exs;
    
    // Guardamos
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error al guardar las exs");
    }];
    
}

-(void) trastearConDatos{
    
    // Creamos una libreta
    AGTNotebook *nb = [AGTNotebook notebookWithName:@"Mi Libreta" context:self.stack.context];
    
    // Creamos notas
    AGTNote *n = [AGTNote noteWithName:@"Una nota"
                               context:self.stack.context];
    
    NSLog(@"Nota: %@", n);
    NSLog(@"Modification date: %@", n.modificationDate);
    
    n.text = @"Hola, hola";
    
    NSLog(@"Modification date: %@", n.modificationDate);
    
    [AGTNote noteWithName:@"otra nota"
                  context:self.stack.context];
    
    [AGTNote noteWithName:@"la última"
                  context:self.stack.context];
    
    
    
    // Búsqueda
    NSFetchRequest *req = [NSFetchRequest
                           fetchRequestWithEntityName:
                           [AGTNote entityName]];
    
    req.fetchLimit = 20;
    req.predicate = nil;
    req.sortDescriptors = @[
                            [NSSortDescriptor
                             sortDescriptorWithKey:AGTNoteAttributes.name ascending:YES],
                            [NSSortDescriptor sortDescriptorWithKey:AGTNoteAttributes.modificationDate ascending:NO]];
    
    NSError *err = nil;
    NSArray *res = [self.stack.context executeFetchRequest:req
                                                     error:&err];
    
    if (res == nil) {
        // Error al buscar
        NSLog(@"Error: %@", err.userInfo);
        
    }else{
        NSLog(@"Número de notas: %d",[res count]);
        NSLog(@"Clase del array de resultados: %@", [res class]);
        
        // asignamos las notas a la libreta
        for (AGTNote *note in res) {
            note.notebook = nb;
        }
    }
    
    // Borramos
    [self.stack.context deleteObject:n];
    
    
    // Guardamos
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Hubo un error al guardar. %@", error.userInfo);
    }];
    
    
    
    
    
}



-(void) autoSave{
    
    // Guardo
    if (AUTO_SAVE) {
        NSLog(@"Auto guardando");
        [self.stack saveWithErrorBlock:^(NSError *error) {
            NSLog(@"Error en autoguardado");
        }];
        
        // Me reenvio dentro de x segundos
        [self performSelector:@selector(autoSave)
                   withObject:nil
                   afterDelay:AUTO_SAVE_DELAY];
    }
}













@end
