#import "AGTNotebook.h"

@interface AGTNotebook ()



@end

@implementation AGTNotebook

+(NSArray*) observableKeys{
    return @[@"name", @"notes"];
}
+(instancetype)notebookWithName:(NSString*)name
                        context: (NSManagedObjectContext *) context{
    
    AGTNotebook *nb = [self insertInManagedObjectContext:context];
    nb.name = name;
    nb.creationDate = [NSDate date];
    nb.modificationDate = [NSDate date];
    
    return nb;
}

#pragma mark - KVO
-(void) setupKVO{
    
    // Alta en notificaciones
    for (NSString *key in [AGTNotebook observableKeys]) {
        [self addObserver:self
               forKeyPath:key
                  options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                  context:NULL];
    }
    
}

-(void) tearDownKVO{
    // Baja en todas las notificaciones
    for (NSString *key in [AGTNotebook observableKeys]) {
        
        [self removeObserver:self
                  forKeyPath:key];
    }
}

// Recepción de la notificación
-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary *)change
                      context:(void *)context{
    // sea quien sea que haya cambiado, actualizo la modificationDate
    self.modificationDate = [NSDate date];
}

#pragma mark - Life cycle
-(void) awakeFromInsert{
    [super awakeFromInsert];
    // Se llama solo una vez, al crear el objeto
    [self setupKVO];
}

-(void) awakeFromFetch{
    [super awakeFromFetch];
    // Se llama cada vez que se recupera el objeto
    [self setupKVO];
}


-(void) willTurnIntoFault{
    [super willTurnIntoFault];
    // Se llama cada vez que un objeto se va a convertir en un fault
    [self tearDownKVO];
}





@end
