//
//  AGTPhotoViewController.m
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 23/01/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTPhotoViewController.h"
#import "AGTNote.h"
#import "AGTPhoto.h"
#import "UIImage+Resize.h"

@interface AGTPhotoViewController ()

@end

@implementation AGTPhotoViewController

-(id) initWithModel:(AGTNote*) note{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = note;
        self.title = note.name;
    }
    return self;
}

- (IBAction)takePicture:(id)sender {
    
    
    // Creamos un UIImagePcker
    UIImagePickerController *p = [UIImagePickerController new];
    
    
    // Configuramos
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        // Tenemos cámara
        p.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        // Pues me conformo con el carrete
        p.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    p.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    // Nos hacemos su delegado
    p.delegate = self;
    
    // Lo presentamos de forma modal
    [self presentViewController:p
                       animated:YES
                     completion:^{
                         // lo que se tiene que hacer despues de mostrarlo
                     }];
    
    

}

- (IBAction)trashPhoto:(id)sender {
    
    CGRect oldBounds = self.photoView.bounds;
    
    // Borramos la fotos
    [UIView animateKeyframesWithDuration:0.7
                                   delay:0
                                 options:0
                              animations:^{
                                  // Lo que cambies aquí se anima
                                  self.photoView.bounds = CGRectZero;
                                  self.photoView.alpha = 0;
                                  self.photoView.transform = CGAffineTransformMakeRotation(M_2_PI);
                              } completion:^(BOOL finished) {
                                  // esto se ejecuta pasados 1.2 segundos y no se anima
                                  self.photoView.image = nil;
                                  self.model.photo.image = nil;
                                  
                                  self.photoView.alpha = 1;
                                  self.photoView.transform = CGAffineTransformIdentity;
                                  self.photoView.bounds = oldBounds;
                                  
                              }];
    
    
    
    
}

-(void) viewDidLoad{
    [super viewDidLoad];
    
    // Nos aseguramos de que la vista no ocupa
    // toda la pantalla (novedad de iOS 7)
    self.edgesForExtendedLayout = UIRectEdgeNone;
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // sincronizo modelo -> vista
    self.photoView.image = self.model.photo.image;
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    // sincornizo vista -> modelo
    self.model.photo.image = self.photoView.image;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    // PICO DE MEMORIA!!!!!
    
    // saco a la gemela del diccionario
    UIImage *gemela = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    // le aplicamos de urgncia una reducción de DD a aa
    // En realidad esto solo es necesario cuando la imagen viene
    // de la cámara
    CGSize newSize = CGSizeMake(gemela.size.width / 2.0, gemela.size.height / 2.0 );
    
    gemela = [gemela resizedImage:newSize
             interpolationQuality:kCGInterpolationHigh];
    
    // la pongo en el modelo
    self.model.photo.image = gemela;
    
    // Quitamos el picker
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 //nada
                             }];
    
}










@end
